#!/usr/bin/env python

# Note: this script expects to be run from this directory,
# with its virtualenv,
# and with PATH correctly set.

from configparser import ConfigParser
from os import path
from shlex import quote
from timeit import default_timer as timer

import sh

import bucket
import cron
import scout_success


def backup_db(config):
    # Does not use sh.pg_dump. Looks like pg_dump seeks on the output file
    # (when using --file AND when redirecting output with "> file", but not
    # with "| cat > file"). So a pipe is required, but there's no way to set
    # pipefail (https://github.com/amoffat/sh/issues?q=pipefail). Also there's
    # no way of extracting the command to be run without running it, so here it
    # is manually built:
    pgdump_cmd = (
        'pg_dump'
        ' --format=custom'
        f' {quote("--dbname=" + config["db"]["name"])}'
        f' {quote("--host=" + config["db"]["host"])}'
        f' {quote("--port=" + config["db"]["port"])}'
        f' {quote("--username=" + config["db"]["user"])}'
    )

    sh.bash('-c',
            f'set -o pipefail; {pgdump_cmd} | cat > {bucket.MNT_PATH}/db.dump',
            _env={'PGPASSWORD': config['db']['password']})


def backup_media(config):
    repo_path = config['project']['repo_path']
    media_path = path.join(repo_path, 'project/media')
    if not path.isdir(media_path):
        # media folder doesn't exist --> project doesn't use media
        return

    sh.tar('czf', f'{bucket.MNT_PATH}/media.tgz', '-C', media_path, '.')


def backup_extra_files(config):
    extra_files_paths = [v for (k, v) in config.items('extra_files')]
    # (Should we use YAML instead of ini?)

    if not extra_files_paths:
        return

    sh.tar('czf', f'{bucket.MNT_PATH}/extra_files.tgz', *extra_files_paths)


def main():
    start_time = timer()

    config = ConfigParser()
    config.read('config.ini')

    bucket.mount(config)
    try:
        cron_schedule = bucket.get_cron_schedule()
        cron.install(cron_schedule)

        backup_db(config)
        backup_media(config)
        backup_extra_files(config)
        scout_success.run(start_time, cron_schedule)
        print('Success.')
    finally:
        bucket.unmount()


if __name__ == '__main__':
    main()
