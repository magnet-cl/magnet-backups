import os

import sh

MNT_PATH = 's3_mnt'


def mount(config):
    prefixed_bucket_name = config['aws']['prefixed_bucket_name']

    os.makedirs(MNT_PATH, exist_ok=True)

    # Unmount if for some reason it's still mounted:
    try:
        unmount()
    except sh.ErrorReturnCode:
        # It could be already umounted, or failed to unmount...
        try:
            sh.findmnt(MNT_PATH,
                       _ok_code=1)  # If nothing mounted here, it's fine
        except sh.ErrorReturnCode:
            # Return code 0:
            raise Exception('Failed to unmount')
    # Improvement: lock? (in case a backup steps over an unfinished one)

    sh.goofys('--profile=magnet-backups',
              prefixed_bucket_name,
              MNT_PATH)


def get_cron_schedule():
    with open(f'{MNT_PATH}/cron.txt', 'r') as cron_config:
        return cron_config.read().strip()


def unmount():
    sh.fusermount('-u', MNT_PATH)
