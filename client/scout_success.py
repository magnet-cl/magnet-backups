from datetime import datetime
import json
import os
import sys
from timeit import default_timer as timer

from croniter import croniter
import sh


def run(start_time, cron_schedule):
    """
    Inform Mothership that this backup was successful, and add data to trigger
    an alert if the next backup is not successfully reported on time.
    """

    data = {
        'elapsed_time': int(timer() - start_time),
        'next_backup_start': int(
            croniter(cron_schedule, datetime.now()).get_next()
        ),
    }

    # Mothership scout uses system's Python, and because this script runs in a
    # virtualenv, it currently fails at "import psutil". To make it
    # independent and future-proof, run Python removing the virtualenv PATH:
    venved_path = os.environ['PATH'].split(':')
    filtered_path = [p for p in venved_path if not p.startswith(sys.prefix)]
    python_cmd = sh.Command('python3', search_paths=filtered_path)

    scout_path = os.path.join(os.environ['HOME'], 'mothership_scout/scout.py')
    python_cmd(scout_path, '--backup-successful', json.dumps(data))
