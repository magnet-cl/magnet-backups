import os
from shlex import quote

from crontab import CronTab, CronSlices

CRON_ID = 'magnet-backups'


def install(schedule):
    if not CronSlices.is_valid(schedule):
        raise Exception(f'Invalid cron schedule: {schedule}')

    with CronTab(user=True) as cron:
        try:
            job = next(cron.find_comment(CRON_ID))
        except StopIteration:
            job = cron.new(
                comment=CRON_ID,
                # This library refuses to work with jobs created with no
                # command:
                command='dummy',
            )

        job.set_command(
            f'cd {quote(os.getcwd())} && '
            'PATH=/bin:/usr/bin:/usr/local/bin/ '
            '/usr/local/bin/pipenv run ./backup.py '
            '> ~/magnet-backups.log 2>&1')  # Save the last execution result
        #                                     for easier debugging of failed
        #                                     jobs.
        job.setall(schedule)
