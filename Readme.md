# Magnet Backups

Backs up the database, media and some extra files of a project.

### Overview

The objective is to backup the data (without code, dependencies or non-specific configurations) of a [DPT project](https://github.com/magnet-cl/django-project-template) to S3. A single bucket holds all backups, with different projects in different subfolders.

For each project, an IAM user is created, with access to its subfolder only. Every time a backup is made, it overwrites the previous one, but thanks to versioning old backups can be retrieved. Very old backups are automatically expired with S3 Lifecycle.

Configuration of what to back up is stored locally. But _when_ to back up is stored in the bucket, so we can change it if we no longer have SSH access.

## Local setup

- Run [magnetizer](https://github.com/magnet-cl/magnetizer)'s quickstart
- `./quickstart.sh`
- [Configure AWS credentials](https://github.com/magnet-cl/magnetizer#aws-playbooks)

This setups Ansible, and allows to use [Magnetizer's inventory handling](https://github.com/magnet-cl/magnetizer#inventory).

### AWS IAM

The configured AWS credentials must have the following managed policies:

- IAMFullAccess: To allow users creation.
- AmazonS3FullAccess: To setup the S3 bucket.

## Install on server

Install `pipenv` on the server (already done if it's running a DPT project), and run in your computer:
```
ansible-playbook -i inventory -l <host> install.yml
```
Use a nice and stable `<host>` (for example `cashflow.prod`) because it's name (`ansible_hostname` --> "cashflow-prod") is used in IAM, folder name, etc.

#### Cron

When the backup script runs, it updates its crontab schedule with the one from `<project subfolder>/cron.txt`, so that file should be manually modified (it's not automated yet) to change schedule.

A schedule is prompted and uploaded by the Ansible playbook if it doesn't exist.

#### Media

The script backs up the `{{repo_path}}/project/media` folder if it exists. So, if media is being stored in S3, delete the local media folder to avoid backing up unnecessary data.

#### Extra files
The "some extra files" that are backed up default to local_settings.py only. You can add and remove files in ~/magnet-backups/client/config.ini, in the \[extra_files] section.

Note that the backup script doesn't run as root, and will fail with "Permission denied" if it tries to backup an unreadable file.

## Notes

- Pipfile (and all client source) is in a subfolder, to prevent `cdenv` from spawning a Pipenv shell, because it breaks Ansible-Magnetizer-inventory ("No module named 'paramiko'")

- In case you want to run another program with this script, note that the `PATH` used by the backup script is hardcoded in Ansible (when it's run the first time for testing) and where the cron job is set.

## AWS setup (for reference only, already done inside of Magnet)

Create an S3 bucket and set its name in `bucket_name` (group_vars/all.yml). Turn on "Block _all_ public access" and enable versioning.

Also create an IAM group called "magnet-backups-operators". It's only used for better organization.

### Lifecycle

Create this single rule for the bucket: (in AWS console, at the root of the bucket, _Management_ tab, _Add lifecycle rule_)

![lifecycle review](readme_images/lifecycle_review.png)

Expiration options that are not shown at review:

![lifecycle review](readme_images/lifecycle_expiration.png)

- Backups less than a week old can be retrieved in real time
- Then they are transitioned to Glacier
- As objects in Glacier are charged for a minimum of 90 days, keep them for 90 days
- The rule applies to all files in the bucket

Ideally, the rule would not apply to configuration files (like cron.txt) (or apply to backup data only), but available filters are just _prefix_ (we need postfix) and _tags_ (not supported by goofys). So, to avoid keeping junk, delete old versions of files that are not required:

| _bad_                                      | _good_                                     |
|:------------------------------------------:|:------------------------------------------:|
| ![dirty](readme_images/versions_dirty.png) | ![clean](readme_images/versions_clean.png) |

An alternative is to create lifecycle rules for each file [with Ansible](https://docs.ansible.com/ansible/latest/modules/s3_lifecycle_module.html), but it would be inconvenient to change file names created by the backup script.

When creating the rule, AWS warns that _Transitioning small objects to Glacier or Glacier Deep Archive will increase costs_, but as there are few files in the bucket, those costs should be low.
