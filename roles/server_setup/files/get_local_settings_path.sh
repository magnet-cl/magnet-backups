#!/bin/bash

# Once I saw a production server with two Django folders in home, both with local_settings.py files.
# So instead of looking for files, look for systemd service.

# Assumptions: a server that's worth backing up has:
# - one Django service running
# - no paths with \n

# Returns the path to local_settings.py, or blank on error.

dir=$(systemctl show --property WorkingDirectory --state=active "django-*.service" | sed -e 's/WorkingDirectory=//')

if [[ -z "$dir" || "$(echo "$dir" | wc -l)" != "1" ]]; then
  # Zero or more than one django-*.service
  exit
fi

local_settings_path=$dir/project/local_settings.py

if [[ ! -f $local_settings_path ]]; then
  # Wrong dir, or local_settings.py not there.
  exit
fi

echo -n "$local_settings_path"
