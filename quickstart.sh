#!/bin/bash

set -e

function print_green(){
    echo -e "\033[32m$1\033[39m"
}

print_green "Downloading inventory script ssh_config.py from ansible contrib"
wget -O inventory/ssh_config.py https://raw.githubusercontent.com/ansible/ansible/stable-2.9/contrib/inventory/ssh_config.py
chmod +x inventory/ssh_config.py

print_green "Setting python3 as interpreter"
sed -i 's/env python$/env python3/' inventory/ssh_config.py

print_green "Installing boto and boto3"
# FIXME: virtualenv...
pip install boto boto3
